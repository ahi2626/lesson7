package com.sourceit.Task2.data;

import com.sourceit.Task2.model.Literature;
import com.sourceit.Task2.model.Book;
import com.sourceit.Task2.model.Magazine;
import com.sourceit.Task2.model.DailyPlanner;


public class Generator {
    public Generator() {
      }

      public Literature[]  generate () {

          Literature[] literature = new Literature[9];
          literature[0] = new Book ( "Преступление и наказание", "Достоевский", "Купельня", 1805 );
          literature[1] = new Book ( "Двойник", "Достоевский", "Знахарь", 1866 );
          literature[2] = new Book ( "Мёртвые души", "Гоголь", "Борода", 1901 );
          literature[3] = new Magazine ( "Ласточка", "Политика", 1902, "февраль" );
          literature[4] = new Magazine ( "Крамсный лиман", "Драматургия", 1910, "март" );
          literature[5] = new Magazine ( "Утко нос", "Литература", 1897, "Январь" );
          literature[6] = new DailyPlanner ( "Первый-Ежегодник", "Политика", "июль", 1916 );
          literature[7] = new DailyPlanner ( "Второй-Ежегодник", "Драматургия", "август", 1917 );
          literature[8] = new DailyPlanner ( "Третий-Ежегодник", "Литература", "сентябль", 1920 );
          return literature;
      }

}
