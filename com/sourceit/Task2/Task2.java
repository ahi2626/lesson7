package com.sourceit.Task2;


import com.sourceit.Task2.model.Literature;

import java.util.Scanner;


public class Task2 {
    public void runTask2() {

        Scanner scan = new Scanner ( System.in );
        System.out.println ( "Что вы хотите найти? (Книга, журнал или ежегодник ):" );
        String DefineLiterature = scan.nextLine ();

        System.out.println ( "Какого года выпуска публикации, выбранной литературы, хотите найти?: " );
        int defineYearOfPublishing = Integer.parseInt ( scan.nextLine () );


        Literature.findAndPrintLiterature ( DefineLiterature  , defineYearOfPublishing );
    }


}


//    String author = "Гоголь";
//        //only books
//        for (Literature literature : literatures) {
//            if (literature instanceof Book) {
//                Book book = (Book) literature;
//                Literature lit = literature;
//                Object object = literature;
//
//                if (book.getAuthor().equalsIgnoreCase(author)) {
//                    System.out.println(book);
//                }
//            }
////            if (literature.isOlderThen(1900)) {
////                System.out.println(literature);
////            }
//        }


//2. Создать структуру данных для хранения информации о книге (название, автор, издательство, год издания),
// о журнале (название, тематика, год и месяц выхода в печать),
// о ежегоднике (название, тематика, издательство, год издания).
// И иметь функционал вывода в консоль литературы (всю доступную информацию) с фильтрацией по определённому году (год вводит пользователь с консоли).


//2.1* Добавить фильтрацию по типу (например: только книги или только журналы)