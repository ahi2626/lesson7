package com.sourceit.Task2.model;

import com.sourceit.Task2.data.Generator;

public class Literature {
 
    private String name;
     private int yearOfPublishing;


    public Literature(String name, int yearOfPublishing) {
        this.name = name;
       this.yearOfPublishing = yearOfPublishing;

    }

    public int getYearOfPublishing( ) {
        return yearOfPublishing;
    }





    @Override
    public String toString() {
        return String.format ( "название:%s, год публикации: %s", name, yearOfPublishing );
    }

    public static void findAndPrintLiterature(String defineLiterature, int defineYearOfPublishing) {
         final String DefineTypeOfBook = "книга";
        final String DefineTypeOfMagazine = "журнал";
        final String DefineTypeOfDailyPlanner = "ежедневник";

        Literature[] literature = new Generator().generate ();

        for ( Literature typeOfLiterature : literature ) {
            if (typeOfLiterature instanceof Book && DefineTypeOfBook.equalsIgnoreCase ( defineLiterature )) {
                Book book = (Book) typeOfLiterature;
                book.findBookByYearOfPublishing ( defineYearOfPublishing );
            } else if (typeOfLiterature instanceof Magazine && DefineTypeOfMagazine.equalsIgnoreCase ( defineLiterature )) {
                Magazine magazine = (Magazine) typeOfLiterature;
                magazine.findMagazineByYearOfPublishing ( defineYearOfPublishing );
            } else if (typeOfLiterature instanceof DailyPlanner && DefineTypeOfDailyPlanner.equalsIgnoreCase ( defineLiterature )) {
                DailyPlanner dailyplanner = (DailyPlanner) typeOfLiterature;
                dailyplanner.findDailyPlannerByYearOfPublishing ( defineYearOfPublishing );
            }
        }


    }
}
