package com.sourceit.Task2.model;

public class Magazine extends Literature {
     private String themeOfMagazine;
    private String monthOfPublishing;




    public Magazine(String name, String theme, int yearOfPublishing, String monthOfPublishing) {
        super ( name, yearOfPublishing );
       this.themeOfMagazine = theme;
        this.monthOfPublishing = monthOfPublishing;
     }

    public void findMagazineByYearOfPublishing(int defineYearOfPublishing) {
        if (defineYearOfPublishing == getYearOfPublishing ()) {
            System.out.println ( String.format ( "Выбранный вами журнал:" + super.toString () + "Тема:%s,месяц издания:%s", themeOfMagazine, monthOfPublishing ) );
        }
    }


}
