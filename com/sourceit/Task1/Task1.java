package com.sourceit.Task1;

import com.sourceit.Task1.model.Bank;
import com.sourceit.Task1.model.Darkmarket;
import com.sourceit.Task1.model.Finance;
import com.sourceit.Task1.model.Obmenka;

import java.util.Scanner;

public class Task1 {

    public static void runTask1() {


        Finance[] finances = new Finance[7];
        finances[0] = new Bank ( "Приватбанк", 26 );
        finances[1] = new Bank ( "Альфабанк", 26.3 );
        finances[2] = new Bank ( "Пумб", 27.1 );
        finances[3] = new Obmenka ( "Обменка №1", 28 );
        finances[4] = new Obmenka ( "Обменка №2", 28.2 );
        finances[5] = new Obmenka ( "Обменка №3", 27.5 );
        finances[6] = new Darkmarket ( "Черный рынок", 26 );

        Scanner scan = new Scanner ( System.in );
        System.out.println ( "Введите сумму денег вам нужно сконвертировать :" );
        Double inputamountofmoney = Double.parseDouble ( scan.nextLine () );

        for ( Finance finance : finances ) {
            if (finance instanceof Bank) {
                Bank bank = (Bank) finance;
                if (bank.checktGrnLimit ( inputamountofmoney )) {
                    bank.convert ( inputamountofmoney );
                }
            }
            if (finance instanceof Obmenka) {
                Obmenka obmenka = (Obmenka) finance;
                if (obmenka.ChecklimitInUsd ( inputamountofmoney )) {
                    obmenka.convert ( inputamountofmoney );
                }
            }


            if (finance instanceof Darkmarket) {
                Darkmarket darkmarket = (Darkmarket) finance;
                   finance.convert ( inputamountofmoney );
                }
            }
     }


}




// Банк имеет лимит в 150 000 грн, а обменник $20.000.
//
// Функционал - запросить у пользователя сумму
// в гривне и вывести в консоль название организации и кол-во валюты после конвертации.
// Если обмен в организации не возможен - указать что операция не допустима.


//  String author = "Гоголь";
//        //only books
//        for (Literature literature : literatures) {
//            if (literature instanceof Book) {
//                Book book = (Book) literature;
//                Literature lit = literature;
//                Object object = literature;
//
//                if (book.getAuthor().equalsIgnoreCase(author)) {
//                    System.out.println(book);
//                }
//            }
////            if (literature.isOlderThen(1900)) {
////                System.out.println(literature);
////            }


//1. Создать структуру данных для хранения информации о банке (название, курс доллара, лимит в гривне),
// об обменнике (название, курс доллара, лимит в долларе),
// о чёрном рынке (одном чёрном рынке, у которого также есть название и курс доллара, лимитов нет).


//1.1* Добавить фильтрацию по типу (например: только банк или только обменка)
//
//
//2. Создать структуру данных для хранения информации о книге (название, автор, издательство, год издания),
// о журнале (название, тематика, год и месяц выхода в печать), о ежегоднике (название, тематика, издательство, год издания).
// И иметь функционал вывода в консоль литературы (всю доступную информацию) с фильтрацией по определённому году (год вводит пользователь с консоли).
//2.1* Добавить фильтрацию по типу (например: только книги или только журналы)