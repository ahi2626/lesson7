package com.sourceit.Task1.model;

public class Darkmarket extends Finance {
    private String name;
    private double courseOfDollar;


    public Darkmarket(String name, double courseOfDollar) {
        super ( name, courseOfDollar );
        this.name = name;
        this.courseOfDollar = courseOfDollar;
    }
}
