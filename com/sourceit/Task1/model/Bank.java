package com.sourceit.Task1.model;

public class Bank extends Finance {
    private String name;
    private double courseOfDollar;
    private double grnLimit = 150000;


    public Bank(String name, double courseOfDollar) {
        super (name, courseOfDollar);
        this.name = name;
        this.courseOfDollar = courseOfDollar;
    }

    public double getGrnLimit() {
        return grnLimit;
    }

    public boolean checktGrnLimit(double amountofmoney) {
        if (amountofmoney < grnLimit) {
            return true;
        } else {
            System.out.println ( String.format ( "Операция невозомжна в банке %s!", super.getName () ) );
            return false;
        }
    }
}
