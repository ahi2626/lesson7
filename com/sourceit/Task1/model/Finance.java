package com.sourceit.Task1.model;

public class Finance {
    private String name;
    private double courseOfDollar;

    public Finance(String name, double courseOfDollar) {
        this.name = name;
        this.courseOfDollar = courseOfDollar;
   }
      public String getName () {
            return name;
    }
  public double getcourseOfDollar () {
            return courseOfDollar;
        }


    public void convert(double amountofmoney) {
      System.out.println(String.format("Ваша сумма по курсу %.2f организации: %s в USD будет %.2f ", courseOfDollar, name, amountofmoney*courseOfDollar));
    }
}


