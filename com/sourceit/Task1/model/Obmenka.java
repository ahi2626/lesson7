package com.sourceit.Task1.model;

public class Obmenka extends Finance {

    private String name;
    private double courseOfDollar;
    private double limitInUsd = 20000;

    public Obmenka(String name, double courseOfDollar) {
        super(name,courseOfDollar);
       this.name = name;
        this.courseOfDollar = courseOfDollar;
  }

   public double getlimitInUsd() {
        return limitInUsd;
    }

    public boolean ChecklimitInUsd(double amountofmoney) {
        double result = amountofmoney * getcourseOfDollar ();
        if (result < limitInUsd) {
            return true;
        } else {
            System.out.println ( String.format ( "Операция в обменнике %s невозможна", super.getName () ) );
            return false;
        }
    }
}